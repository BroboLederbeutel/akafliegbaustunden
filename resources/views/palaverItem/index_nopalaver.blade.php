@extends('layouts.layout')
@section('title','TODO')
@section('head')

@endsection

@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <div class="col-md-12">
        <?php $counter = 0?>
        <?php $max = 0;
        foreach ($projects as $project) {
            $max += $project->palaverItems->count(); //amount of palaverItems dispplayed
        }?>
        @foreach($projects as $project)

            <label for="{{$project->name}}_link" class="col-md-12 col-xs-12 project-title"
                   style="z-index: 100; position: relative">
                <div class="panel panel-default col-md-12 col-md-offset-0 ">
                    <a href="/project/{{$project->id}}/edit?type=np" style="color: black" id="{{$project->name}}_link">
                        <h2 class="col-md-10">{{$project->name}}</h2>
                    </a>
                </div>
            </label>


            @foreach($project->palaverItems as $palaverItem)
                <div class="col-md-10 col-md-offset-1">
                    <div class="panel panel-default {{$palaverItem->id}}_idnav" id="div_{{$counter}}">
                        <div class="panel-heading"
                             @if($palaverItem->done)
                             style="background-color: green"
                                @endif
                        >
                            <a href="/palaverItem/{{$palaverItem->id}}/edit?type=np" style="color: black;">
                                <h4>{{$palaverItem->title}}</h4>
                            </a>
                        </div>
                        <div class="panel-body" id={{$palaverItem->id}}>


                            <div class="row col-md-6">
                                <dl class="dl-horizontal ">

                                    <h4>
                                        <dt>Verantwortlich</dt>
                                        <dd id="{{$palaverItem->id}}_users">{{$palaverItem->responsible_users}}</dd>

                                        <dt>Beschreibung</dt>
                                        <dd id="{{$palaverItem->id}}_description">{{$palaverItem->description}}</dd>

                                        <dt>Gesamtbauzeit</dt>
                                        <dd>{{$palaverItem->formatted_work_time}}</dd>
                                        <dt>Aktueller Status</dt>
                                        <dd id={{$palaverItem->id}}_status>{{$palaverItem->real_status}}</dd>

                                        @if(isset($palaverItem->date))
                                            <dt>Termin</dt>
                                            <dd id={{$palaverItem->id}}_termin>{{$palaverItem->date}}</dd>
                                        @endif

                                    </h4>
                                </dl>
                            </div>
                            <div class="row col-md-5 col-md-offset-1">
                                @if(!$palaverItem->entries->isEmpty())
                                    <table class="table table-hover  table-bordered">
                                        @foreach($palaverItem->entries->sortBy('date')->chunk(3)[0] as $entry)
                                            <tr>
                                                <td>{{$entry->user->short_name}}</td>
                                                <td> {{$entry->description}}</td>
                                            </tr>
                                        @endforeach
                                    </table>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <?php $counter++?>
            @endforeach
            <div class="row  col-md-10 col-md-offset-1">


                <a href="{{url('/palaverItem/create?project='.$project->id.'&type=np')}}"
                   class="btn btn-warning col-md-offset-5">
                    <h5>
                        Weiteren Eintrag
                        erstellen</h5></a>

            </div>
        @endforeach

    </div>
@endsection

@section('afterBody')
    <script>
        $(document).ready(function () {
            @if(Session::has('hook'))
            console.log("hit");
            window.scrollTo(0, $('.{{Session::get('hook')}}_idnav').offset().top - 100);
            // document.getElementsByClassName({{Session::get('hook')}}+"_idnav")[0].scrollIntoView();

            @endif
        });

    </script>

@endsection
