@extends('layouts.layout')
@section('title','Anwesenheit')

@section('content')
    <h2>Anwesenheit</h2>
    <div class="col-md-12">
        {!!  Form::open(['method'=>'post','url' => 'palaver/create']) !!}
        <table class="table table-hover  table-bordered">
            <thead>
            <tr>
                <td>Name</td>
                <td>Anwesend</td>
                <td>Entschuldigt</td>
                <td>Abwesend</td>

            </tr>
            </thead>

            @include('palaver.form_partial')

        </table>
        {!! Form::submit('Speichern',['class' => 'btn btn-primary col-md-2 col-md-offset-5']) !!}
        {!! Form::close() !!}
    </div>
@endsection