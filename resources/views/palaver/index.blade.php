@extends('layouts.layout')
@section('title','PALAVER!')

@section('content')
    <div class="col-md-12">
        <table class="table table-hover  table-bordered">
            <thead>
            <tr>
                <td>Datum</td>
                <td>Anwesend</td>
                <td>Entschuldigt</td>
                <td>Abwesend</td>
            </tr>
            </thead>

            @foreach($palavers as $palaver)
                <tr>
                    <td class="col-md-1">
                        <a href="/palaver/{{$palaver->id}}">{{$palaver->date}}</a>
                    </td>
                    <td class="col-md-1">
                        {{collect(unserialize($palaver->anwesenheit)[0])->count()}}
                    </td>
                    <td class="col-md-1">
                        {{collect(unserialize($palaver->anwesenheit)[1])->count()}}
                    </td>
                    <td class="col-md-1">
                        {{collect(unserialize($palaver->data)[1])->count() - collect(unserialize($palaver->anwesenheit)[1])->count() - collect(unserialize($palaver->anwesenheit)[0])->count()}}
                    </td>
                </tr>
            @endforeach

        </table>
    </div>

@endsection