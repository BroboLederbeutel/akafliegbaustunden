@extends('layouts.outer_form_partial')
@section('title','Eintrag hinzufügen')

@yield('content')

@section('panel_heading','Eintrag erstellen')

@section('panel_body')
        {!!  Form::open(['method'=>'post','url' => 'entry/','class' => 'form-horizontal']) !!}
        @include('entry.form_partial')
        @include('layouts.form_buttons_partial')

        <script>
            $('#btnReset').click(function () {
                $(".selectpicker").selectpicker('deselectAll');
                $('#helper_multiselect').selectpicker('refresh');
            })
        </script>


        {!! Form::close() !!}
@endsection

