@extends('layouts.layout')
@section('content')
    @if(env('ALLOW_PAST_YEARS'))
        <select id="year-select" class="form-control">
            @for($x=Help::currentYear(); $x>=2017; $x--)
                <option value="{{$x}}"
                        @if($x == $year)
                        selected
                        @endif
                >{{$x-1}}/{{$x}}</option>
            @endfor
        </select>


        @push('footer')
        <script>
            $(document).ready(function () {
                $('#year-select').change(function () {
                    window.location = '/statistics' + '?year=' + $('#year-select').val();
                });
            });
        </script>
        @endpush
    @endif
    {!! Statistic::getWorkHoursByProjekt($year)!!}

    <h3>Arbeitszeit aller Aktiven: {!! Statistic::getAktiveWorkTime($year) !!}
        <br>
        Arbeitszeit aller Anwärter: {!! Statistic::getAnwaerterWorkTime($year) !!}
        <br></h3>
    <h2>50 % mehr Arbeitszeit als Durchschnitt</h2>
    {!! Statistic::getWayAboveAverageUsers($year) !!}
    <h2>50 % weniger Arbeitszeit als Durchschnitt</h2>
    {!! Statistic::getWayBelowAverageUser($year) !!}

    <h3>Wer arbeitet an welchem Projekt</h3>
    <table class="table">
        <thead>
        <tr>
            <td>Projekt</td>
            <td>Name</td>
            <td>Zeit</td>
        </tr>
        @foreach($stats as $stat)
            <tr>
                <td>
                    {{$stat->project}}
                </td>
                <td>
                    @if(empty($stat->nickname))
                        {{$stat->first_name}}
                    @else
                        {{$stat->nickname}}
                    @endif
                </td>
                <td>
                    {{Help::format_time($stat->time)}}
                </td>
            </tr>
        @endforeach
        <tr></tr>
        </thead>


    </table>

    {{--    //wer arbeitet in welchen Bereichen
        //Offene vs abgeschlossene Projekte
        //durchschnittliche Projektdauer
        //Palaveranwesenheit--}}


@endsection
