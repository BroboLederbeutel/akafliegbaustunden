
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>Flugzeug</th>
            <th>Flugbereit</th>

        </tr>
        </thead>
        <tbody>

        @foreach($airplanes as $airplane)
            <tr>
                <td><a href="/project/{{$airplane->id}}" style="color: #333;">{{$airplane->name}}</a></td>
                <td>
                    @if($airplane->description!="")
                        <span data-toggle="tooltip" title="{{$airplane->description}}">
                            <span class="glyphicon glyphicon-info-sign" aria-hidden="true">
                            </span>
                        </span>
                    @endif
                    <div class="pull-right">{{$airplane->flight_ready_string}}</div>
                </td>
        @endforeach
        </tbody>
    </table>
