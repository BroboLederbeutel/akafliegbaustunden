@extends('layouts.outer_form_partial')
@section('title')
    Mitglied Bearbeiten
@endsection


@section('panel_heading','Mitglied bearbeiten')

@section('panel_body')
    {!!  Form::model($user,['url'=>'user/'.$user->id,'method'=>'patch','class' =>'form-horizontal','role'=>'form']) !!}


    @include('user.form_partial')

    @include('layouts.form_buttons_partial')

    {!! Form::close() !!}

@endsection
