@extends('layouts.outer_form_partial')
@section('title','Projekt bearbeiten')


@section('panel_heading','Projekt bearbeiten')

@section('panel_body')

    @if(isset($_REQUEST['type']))
        {!!  Form::model($project,['method'=>'patch','url' => 'project/'.$project->id.'?type='.$_REQUEST['type'],'class' =>'form-horizontal','role'=>'form']) !!}
    @else
        {!!  Form::model($project,['method'=>'patch','url' => 'project/'.$project->id,'class' =>'form-horizontal','role'=>'form']) !!}
    @endif

    @include('project.form_partial')

    @include('layouts.form_buttons_partial')


    {!! Form::close() !!}

@endsection

