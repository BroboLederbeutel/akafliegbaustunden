<!-- $palaverItem variable  must  be defined!! -->

<a href="{{url('/palaverItem/'.$palaverItem->id)}}" style="color: black;text-decoration: none">
    <div class="panel panel-default" style="height: 215px; overflow:auto ">
        <div class="panel-heading"
             @if($palaverItem->canceled)
             id="gradRed"
             @elseif($palaverItem->done)
             id="gradGreen"
                @endif
        >
            <h3 class="panel-title">{{$palaverItem->title}}</h3>
        </div>
        <div class="panel-body" style="min-height: 150px;">
            <dl class="dl-horizontal">

                <dt>Verantwortlich</dt>
                <dd>{{$palaverItem->responsible_users}}</dd>

                @if($palaverItem->active)
                    <dt>Status</dt>
                    <dd style="max-height: 65px; overflow:auto ">
                        @if(strlen($palaverItem->status)>0)
                            {{$palaverItem->status}}
                        @else
                            ---
                        @endif
                    </dd>
                    @unless(empty($palaverItem->date))
                    <dt>Termin</dt>
                    <dd>{{\Carbon\Carbon::parse($palaverItem->date)->format('d.m.Y')}}</dd>
                    @endunless
                    <dt>Arbeit diese Woche</dt>
                    <dd>{{Help::format_time($palaverItem->work_this_week)}}</dd>
                    <dt>Arbeit diesen Monat</dt>
                    <dd>{{Help::format_time($palaverItem->work_this_month)}}</dd>
                    <dt>Arbeit dieses Jahr</dt>
                    <dd>{{Help::format_time($palaverItem->work_this_year)}}</dd>
                @else
                    <dt>Gesamtarbeitszeit</dt>
                    <dd>{{Help::format_time($palaverItem->total_work_time)}}</dd>
                    @if($palaverItem->canceled)
                        <h3 style="text-align: center">Aufgabe gescheitert</h3>
                    @elseif($palaverItem->done)
                        <h3 style="text-align: center">Aufgabe erfolgreich abgeschlossen</h3>
                    @endif
                @endif

            </dl>
        </div>
    </div>
</a>