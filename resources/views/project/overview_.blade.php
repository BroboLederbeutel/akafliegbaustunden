<!-- $project variable  must  be defined!! -->
<a href="{{url('/project/'.$project->id)}}" style="color: black;text-decoration: none">
    <div class="panel panel-default" style="height: 230px; overflow:auto ">
        <div class="panel-heading">
            <h3 class="panel-title">{{$project->name}}</h3>
        </div>
        <div class="panel-body" style="min-height: 150px;">
            <dl class="dl-horizontal">

                <dt>Verantwortlicher</dt>
                <dd>{{$project->supervisor_string}}</dd>
                @if($project->isAirplane)
                    <dt>Flugklar</dt>
                    <dd>{{$project->flight_ready_string}}</dd>

                @endif
                <dt >Offene Aufgaben</dt>
                <dd style="max-height: 70px; overflow:auto ">
                    @if($project->open_issues()->count() == 0)
                        Keine
                    @else
                        @foreach($project->open_issues() as $palaverItem)
                            {{$palaverItem->title}},
                        @endforeach
                    @endif
                </dd>
                <dt>Arbeit diese Woche</dt>
                <dd>{{Help::format_time($project->work_this_week)}}</dd>
                <dt>Arbeit diesen Monat</dt>
                <dd>{{Help::format_time($project->work_this_month)}}</dd>
                <dt>Arbeit dieses Jahr</dt>
                <dd>{{Help::format_time($project->work_this_year)}}</dd>

            </dl>
        </div>
    </div>
</a>
