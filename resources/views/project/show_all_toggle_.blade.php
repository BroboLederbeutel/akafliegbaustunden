@if(isset($show_all)&&$show_all)
    <a href='/project?show_all=0'>
        <button class="btn btn-default" style="width: 200px">
            Alte Einträge ausblenden <span class="glyphicon glyphicon-minus" style="vertical-align:middle"></span>
        </button>
    </a>

@else
    <a href='/project?show_all=1'>
        <button class="btn btn-default" style="width: 200px">
            Alte Einträge anzeigen
            <span class="glyphicon glyphicon-time"></span>
        </button>
    </a>
@endif
