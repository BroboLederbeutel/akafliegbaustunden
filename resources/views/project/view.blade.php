@push('head')
<style>
    #gradGreen {

        background: -webkit-linear-gradient(right, rgba(255,0,0,0), green); /* For Safari 5.1 to 6.0 */
        background: -o-linear-gradient(left, rgba(255,0,0,0), green); /* For Opera 11.1 to 12.0 */
        background: -moz-linear-gradient(left, rgba(255,0,0,0), green); /* For Firefox 3.6 to 15 */
        background: linear-gradient(to left, rgba(255,0,0,0), green); /* Standard syntax (must be last) */
    }

    #gradRed {

        background: -webkit-linear-gradient(right, rgba(255,0,0,0), red); /* For Safari 5.1 to 6.0 */
        background: -o-linear-gradient(left, rgba(255,0,0,0), red); /* For Opera 11.1 to 12.0 */
        background: -moz-linear-gradient(left, rgba(255,0,0,0), red); /* For Firefox 3.6 to 15 */
        background: linear-gradient(to left, rgba(255,0,0,0), red); /* Standard syntax (must be last) */
    }

</style>
@endpush
@extends('layouts.layout')

@section('title','Übersicht')

@section('content')


    <h1 style="text-align: center"> {{$project->name}}</h1>
    <div class="col-md-12"> @include('project.show_all_toggle_')</div>
    <div class="col-md-12">
        <a href='/project/{{$project->id}}/edit'>
            <button class="btn btn-default" style="width: 200px">
                Bearbeiten <span class="glyphicon glyphicon-pencil" style="vertical-align:middle"></span>
            </button>
        </a>
    </div>
    <div class="col-md-12">

        @can('create',[\App\models\palaverItem::class,$project])
            <a href='/palaverItem/create?project={{$project->id}}'>
                <button class="btn btn-default" style="width: 200px;margin-bottom: 10px">
                    Aufgabe hinzufügen <span class="glyphicon glyphicon-plus" style="vertical-align:middle"></span>
                </button>
            </a>
        @endcan
    </div>

    <div class="row">
        <div class="panel panel-default col-md-8 col-md-offset-2">
            <div class="panel-body">
                <dl class="dl-horizontal ">
                    <h4>
                        <dt>Verantwortlich</dt>
                        <dd>{{$project->supervisor_string}}</dd>
                        @if($project->isAirplane)
                            <dt>Flugklar</dt>
                            <dd>{{$project->flight_ready_string}}</dd>
                        @endif
                        <dt>Arbeit Woche</dt>
                        <dd>{{Help::format_time($project->work_this_week)}}</dd>
                        <dt>Arbeit Monat</dt>
                        <dd>{{Help::format_time($project->work_this_month)}}</dd>
                        <dt>Arbeit Jahr</dt>
                        <dd>{{Help::format_time($project->work_this_year)}}</dd>
                    </h4>
                </dl>
            </div>
        </div>
    </div>


    <div class="row">
        @foreach($project->palaverItems as $palaverItem)
            <div class="col-md-6">
                @include('project.palaverItem_overview_')
            </div>
        @endforeach
    </div>

    <div class="row">
        <h3>Am Projekt beteiligt:</h3>
        {!! Statistic::getProjectHelpers($project->id)!!}
    </div>
@endsection
