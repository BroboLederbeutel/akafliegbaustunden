@if(isset($show_all_users) && $show_all_users==1)
    <a href=/user?show_all_users=0>

        <button class="btn btn-default">
            Ausgetretene Ausblenden <span class="glyphicon glyphicon-eye-close" style="vertical-align:middle"></span>
        </button>
    </a>
    @else
    <a href=/user?show_all_users=1>
        <button class="btn btn-default">
            Ausgetretene Anzeigen
            <span class="glyphicon glyphicon-eye-open" style="vertical-align:middle"></span>
        </button>
    </a>
@endif