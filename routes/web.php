<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use App\models\user;

URL::forceRootURL(env('APP_URL'));

//Enable logout via get
Route::get('logout', function (){
    Auth::logout();
    return redirect('/login');
});

Route::get('datenschutz', function () {
    return view('datenschutz');
});



Route::group(['middleware' => 'auth'], function () {
    Route::get('/', function () {  //redirect to create entry
        return redirect('/entry/create');
    });

    Route::get('/email_overview', 'user_controller@show_email_overview');

//Route::get('/summary', 'summary_controller@display');

    Route::resource('user', 'user_controller', ['except' => ['create', 'store']]);
    Route::resource('entry', 'entry_controller');
    Route::resource('project', 'project_controller');
    Route::resource('palaverItem', 'palaver_controller');
    Route::resource('pilot', 'pilot_controller', ['only' => ['destroy', 'create', 'store']]);
//Route::post('/home', 'HomeController@index');

    Route::post('/palaverItem/{palaverItem}/ajax', 'ajax_controller@palaver');

    Route::get('/fliegen', function (\Illuminate\Http\Request $request) {
        $user = "";
        if (isset($request->date)) {
            $date = \Carbon\Carbon::parse($request->date)->toDateString();
        } else {
            $date = \Carbon\Carbon::now('Europe/Berlin')->addDay(1)->subHour(9)->toDateString(); //Neuer Tag wird immmer ab 9 Uhr angezeigt
        }
        //Flugberechtigte Nutzer Filtern
        $user = User::withoutGlobalScope('jungeGruppe')->with('versions', 'status', 'entries')->get()->filter(function (user $item) {
            return $item->getFlugberechtigtAttribute(Help::currentYear()) || $item->status->name == 'Alter Sack';
        });


        return view('fliegen.index')->with('pilots', \App\models\pilot::where('date', $date)->get())->with('cars', \App\models\car::where('date', $date)->get())->with('airplanes', \App\models\project::withoutGlobalScope('EDEP')->where('isAirplane',1)->where('active','1')->get())->with('date', $date)->with('users', $user);
    });
    /*
        Route::post('/car/create', function (\Illuminate\Http\Request $request) {

            $car = \App\models\car::create($request->except(['_token']));
            $car->save();

            return back();

        });*/
    Route::post('/car/create','car_controller@store');
    Route::delete('/car/{car}','car_controller@destroy');
    Route::put('/car/{car}','car_controller@addPassenger');

    Route::delete('/car/{car}/{user}','car_controller@removePassenger');

    Route::post('/fliegen/ajax', 'ajax_controller@fliegen');

// Authentication Routes...
    Route::get('logout', 'Auth\LoginController@logout');

// Registration Routes...
    Route::get('/register', 'user_controller@create');
    Route::post('/register', 'user_controller@store');


    if(env('PALAVER_FEATURES')) {
        Route::get('/palaverIndex', 'palaver_controller@palaverIndex');
        Route::get('/pdf', 'palaver_controller@pdf');
        Route::get('/palaver/create', 'palaver_controller@create_palaver');
        Route::post('palaver/create', 'palaver_controller@save_anwesenheit');
        Route::get('/palaver/{palaver}/edit', 'palaver_controller@editAnwesenheit');
        Route::post('/palaver/{palaver}/edit', 'palaver_controller@updateAnwesenheit');
        Route::get('/palaver/{palaver}', 'palaver_controller@pdf');
        Route::get('palaver', 'palaver_controller@create_anwesenheit');
        Route::post('palaver_save_data', 'palaver_controller@save_data');
        Route::get('/palaver_view', 'palaver_controller@palaver');

    }

});

Route::get('/loginAs/{user}', function (User $user) {
    if (Auth::user()->id == 1) {
        Auth::logout();
        Auth::loginUsingId($user->id);
        return redirect('/entry/create');
    } else {
        dd("GTFO");
    }
});





Route::get('login', 'Auth\LoginController@showLoginForm');
Route::post('login', 'Auth\LoginController@login');


Route::get('statistics', function (\Illuminate\Http\Request $request) {
    if (env('SHOW_STATISTICS') || Auth::user()->is_admin) {
        if (env('ALLOW_PAST_YEARS')) {
            $year = $request->year;
            if (empty($year)) {
                $year = Help::currentYear();
            }
        } else {
            $year = Help::currentYear();
        }

        $statistic = DB::select("SELECT users.first_name, users.nickname, SUM(entries.work_time) AS time, projects.name AS project from users JOIN entries on users.id = entries.user_id JOIN palaverItems on entries.palaverItem_id = palaverItems.id JOIN projects on palaverItems.project_id = projects.id GROUP BY users.id,projects.id
ORDER BY projects.id");

        return view('statistics')->with('year', $year)->with('stats', $statistic);
    } else {
        abort(403, "Statistik deaktiviert, hier gibts nix zu sehen");
    }
});

