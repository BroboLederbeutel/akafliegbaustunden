<?php

namespace Tests\Feature;

use App\Http\Controllers\entry_controller;
use App\Http\Requests\entry_request;
use App\models\entry;
use App\models\User;
use App\Policies\entryPolicy;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\Access\Gate;
use Illuminate\Auth\Access\Response;
use Illuminate\Auth\AuthServiceProvider;
use Mockery\Mock;
use Tests\TestCase;

class entryAuthorizationTest extends TestCase
{

    public function boot(GateContract $gate)
    {

    }

    public function testAuthorizeIsCalled()
    {

        // TODO testen ob die policy auch wirklich aufgerufen wird
        $entry = factory(entry::class)->make();
        $request = factory(entry_request::class, 'valid')->make();
        $uut = \Mockery::mock(entry_controller::class)->makePartial();

        //store
        $uut->shouldReceive('authorize')->once()->andReturn(True);
        $uut->store($request);
        //update
        $uut->shouldReceive('authorize')->once()->andReturn(True);
        $uut->update($request, $entry);

        //destroy
        $uut->shouldReceive('authorize')->once()->andReturn(True);
        $uut->destroy($entry);

    }

    public function testUserMayCreateEntriesforHimself()
    {
        /* @var \App\models\User $user */
        $user = factory(User::class)->make();
        $user->admin = 0;

        /* @var \App\models\entry $entry */
        $entry = factory(entry::class)->make();
        $entry->user_id = $user->id;

        $uut = \Mockery::mock(entryPolicy::class)->makePartial();


        /* @var Response $response */
        $response = $uut->changing($user, $entry);
        self::assertInstanceOf(Response::class, $response);
        self::assertEquals('granted', $response->message());
    }

    public function testUserMayNotModifyEntriesForOthers()
    {
        /* @var \App\models\User $user */
        $user = factory(User::class)->make();

        /* @var \App\models\entry $entry */
        $entry = factory(entry::class)->make();
        $entry->user_id = $user->id + 1;
        $uut = \Mockery::mock(entryPolicy::class)->makePartial();

        $this->expectException(AuthorizationException::class);
        $uut->changing($user, $entry);
    }

    public function testAdminCanModifyAllEntries()
    {
        /* @var \App\models\User $user */
        $user = factory(User::class)->make();
        $user->admin = 1;
        /* @var \App\models\entry $entry */
        $entry = factory(entry::class)->make();
        $entry->user_id = $user->id + 1;

        $uut = \Mockery::mock(entryPolicy::class)->makePartial();

        //entries from others
        $response = $uut->changing($user, $entry);
        self::assertInstanceOf(Response::class,$response);
        self::assertEquals('granted', $response->message());

        $entry = factory(entry::class)->make();
        $entry->user_id = $user->id;

        // his own entries
        /* @var Response $response */
        $response = $uut->changing($user, $entry);
        self::assertInstanceOf(Response::class,$response);
        self::assertEquals('granted', $response->message());

    }

    // NOTE: valid date Range is enforced by the entry_request, the test is therefore in entryRequestTest

    // Alles wird hierauf zurückgeführt
    public function testChangingMethodIsCalled()
    {
        $user = factory(User::class)->make();
        $user->admin = 1;
        /* @var \App\models\entry $entry */
        $entry = factory(entry::class)->make();
        $uut = \Mockery::mock(entryPolicy::class)->makePartial();
        $uut->shouldReceive('changing')->with($user, $entry)->times(4)->andReturn(Null);

        $uut->edit($user, $entry);
        $uut->destroy($user, $entry);
        $uut->store($user, $entry);
        $uut->update($user, $entry);

    }

/*
    public function testTest()
    {
        $uut = \Mockery::mock(entryPolicy::class)->makePartial();
       \Illuminate\Support\Facades\Gate::policy(entry::class, $uut);
      $uut->shouldReceive('changing')->once()->andReturn();
        $user = factory(User::class)->make();
        $user->admin = 1;
         @var \App\models\entry $entry
        $entry = factory(entry::class)->make();
        $controler = new entry_controller();
       

        \Illuminate\Support\Facades\Gate::shouldReceive('changing', \Mockery::any(), $entry)->once()->andReturn();
        \Illuminate\Support\Facades\Gate::makePartial();
        \Auth::login($user);
        $controler->update(factory(entry_request::class,'valid')->make(), $entry);

    }
    */

}


