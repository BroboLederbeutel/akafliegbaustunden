<?php

namespace Tests\Feature;

use App\models\entry;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\RedirectResponse;
use Tests\TestCase;
use App\Http\Requests\entry_request;
use App\Http\Controllers\entry_controller;


class entryControllerTest extends TestCase
{


    public function testCreation()
    {
        /* @var entry_request $request */
        $request = factory(entry_request::class, 'valid')->make();

        $mock = \Mockery::mock(entry_controller::class)->makePartial();
        $mock->shouldReceive('authorize')->with('store', \Mockery::any())->once()->andReturn(True);

        $response = $mock->store($request);


        self::assertInstanceOf(RedirectResponse::class, $response);
        self::assertEquals($response->getSession()->get('alert-success'), 'Eintrag gespeichert');
        self::assertInstanceOf(entry::class, entry::whereDescription($request->get('description'))->first());

        //cleanup
        self::assertTrue(entry::whereDescription($request->get('description'))->first()->delete());
    }


    public function testDeletion()
    {

        $uut = \Mockery::mock(entry_controller::class)->makePartial();
        $uut->shouldReceive('authorize')->with('destroy', entry::class)->once()->andReturn(True);

        $entry = factory(entry::class)->create();
        self::assertInstanceOf(entry::class, entry::findOrFail($entry->id));
        self::assertInstanceOf(\Symfony\Component\HttpFoundation\RedirectResponse::class, $uut->destroy($entry));

        $this->expectException(ModelNotFoundException::class);
        entry::findOrFail($entry->id);

    }

    public function testEdit()
    {
        //TODO
        /* @var entry $entry */
        $entry = factory(entry::class)->create();

        $uut = \Mockery::mock(entry_controller::class)->makePartial();
   //   $uut->shouldReceive('authorize')->with(entry::class)->once()->andReturn(True);

        $this->followingRedirects()->get('/entry/'.$entry->id.'/edit')->assertOk();




    }




}
