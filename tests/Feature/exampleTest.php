<?php

namespace Tests\Feature;

use App\models\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class exampleTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testBasicExample()
    {
        $response = $this->get('/');

        $response->assertStatus(302);
    }

    public function testExample()
    {
        $user = factory(\App\models\User::class)->make();
        $user->admin = 1;

        $this->followingRedirects()->actingAs($user)->get('/')
            ->assertSee('Eintrag hinzufügen');
    }
}
