<?php

namespace Tests\Browser;

use App\models\User;
use Tests\TestCase;


/* This class is meant to test all available routes for basic availability until tests for all models are available*/
class test_routes extends TestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testStart()
    {
       $response = $this->get('/');
       $response->assertStatus(302);

    }

    public function testBaustundenEintragen()
    {
        $response = $this->actingAs(User::firstOrFail())->get('/entry/create');
        $response->assertOk();
    }

    public function testMeineBaustunden()
    {
        $response = $this->actingAs(User::firstOrFail())->get('/user/'.User::firstOrFail()->id);
        $response->assertOk();
    }

    public function testUebersicht()
    {
        $response = $this->actingAs(User::firstOrFail())->get('/project');
        $response->assertOk();
    }

    public function testFliegen()
    {
        $response = $this->actingAs(User::firstOrFail())->get('/fliegen');
        $response->assertOk();
    }

    public function testUser()
    {
        $response = $this->actingAs(User::whereAdmin(1)->first())->get('/user');
        $response->assertOk();
    }

}
