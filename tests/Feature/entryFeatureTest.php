<?php

namespace Tests\Browser;

use App\models\entry;
use App\models\User;
use Faker\Factory;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;

class entryFeatureTest extends DuskTestCase
{



    public function testCreate()
    {
        $this->browse(function (Browser $browser) {
            $user = factory(User::class)->create();


            $faker = Factory::create();
            $browser->loginAs($user);
            $browser->assertAuthenticatedAs($user);

            $browser->visit('entry/create')
                ->assertSee('erstellen');

            $text = $faker->text(300).' new';
            $browser->loginAs($user->id)->visit('entry/create')
                ->type('description', $text)
                //     ->type('date', Carbon::yesterday()->toDateString())
                ->select('palaverItem_id')
                ->select('helper_id[]', User::inRandomOrder()->get()->first()->id)
                ->type('hours',2)
                ->type('minutes',1)
                ->press('Speichern')
                ->assertSee('Eintrag gespeichert')
                ->assertSee('Arbeitszeit')
                ->assertSee('Eintrag erstellen');

            $newEntry = entry::whereDescription($text)->first();
            self::assertNotNull($newEntry);
            self::assertEquals($text, $newEntry->description);

        });
    }

    public function testCreate_overflow_minutes()
    {
        $this->browse(function (Browser $browser) {
            $user = factory(User::class)->create();


            $faker = Factory::create();
            $browser->loginAs($user);
            $browser->assertAuthenticatedAs($user);

            $browser->visit('entry/create')
                ->assertSee('erstellen');

            $text = $faker->text(300).' new';
            $browser->loginAs($user->id)->visit('entry/create')
                ->type('description', $text)
                //     ->type('date', Carbon::yesterday()->toDateString())
                ->select('palaverItem_id')
                ->select('helper_id[]', User::inRandomOrder()->get()->first()->id)
                ->type('hours',1)
                ->type('minutes',120)
                ->press('Speichern')
                ->assertSee('Eintrag gespeichert')
                ->assertSee('Arbeitszeit')
                ->assertSee('Eintrag erstellen');

            $newEntry = entry::whereDescription($text)->first();
            self::assertNotNull($newEntry);
            self::assertEquals($text, $newEntry->description);
            self::assertEquals($newEntry->work_time,180);

        });
    }

    public function testCreate_empty_minutes()
    {
        $this->browse(function (Browser $browser) {
            $user = factory(User::class)->create();


            $faker = Factory::create();
            $browser->loginAs($user);
            $browser->assertAuthenticatedAs($user);

            $browser->visit('entry/create')
                ->assertSee('erstellen');

            $text = $faker->text(300).' new';
            $browser->loginAs($user->id)->visit('entry/create')
                ->type('description', $text)
                //     ->type('date', Carbon::yesterday()->toDateString())
                ->select('palaverItem_id')
                ->select('helper_id[]', User::inRandomOrder()->get()->first()->id)
                ->type('hours',2)
                ->press('Speichern')
                ->assertSee('Eintrag gespeichert')
                ->assertSee('Arbeitszeit')
                ->assertSee('Eintrag erstellen');

            $newEntry = entry::whereDescription($text)->first();
            self::assertNotNull($newEntry);
            self::assertEquals($text, $newEntry->description);

        });
    }

    public function testCreate_empty_hours()
    {
        $this->browse(function (Browser $browser) {
            $user = factory(User::class)->create();


            $faker = Factory::create();
            $browser->loginAs($user);
            $browser->assertAuthenticatedAs($user);

            $browser->visit('entry/create')
                ->assertSee('erstellen');

            $text = $faker->text(300).' new';
            $browser->loginAs($user->id)->visit('entry/create')
                ->type('description', $text)
                //     ->type('date', Carbon::yesterday()->toDateString())
                ->select('palaverItem_id')
                ->select('helper_id[]', User::inRandomOrder()->get()->first()->id)
                ->type('minutes',12)
                ->press('Speichern')
                ->assertSee('Eintrag gespeichert')
                ->assertSee('Arbeitszeit')
                ->assertSee('Eintrag erstellen');

            $newEntry = entry::whereDescription($text)->first();
            self::assertNotNull($newEntry);
            self::assertEquals($text, $newEntry->description);

        });
    }

    public function testCreate_empty_time()
    {
        $this->browse(function (Browser $browser) {
            $user = factory(User::class)->create();


            $faker = Factory::create();
            $browser->loginAs($user);
            $browser->assertAuthenticatedAs($user);

            $browser->visit('entry/create')
                ->assertSee('erstellen');

            $text = $faker->text(300).' new';
            $browser->loginAs($user->id)->visit('entry/create')
                ->type('description', $text)
                //     ->type('date', Carbon::yesterday()->toDateString())
                ->select('palaverItem_id')
                ->select('helper_id[]', User::inRandomOrder()->get()->first()->id)
                ->press('Speichern')
                ->assertSee('Willst du was eintragen musst du auch was machen')
                ->assertSee('Arbeitszeit')
                ->assertSee('Eintrag erstellen');

            $newEntry = entry::whereDescription($text)->first();
            self::assertNull($newEntry);

        });
    }

    public function testCreate_negative_time_regular_user()
    {
        $this->browse(function (Browser $browser) {
            $user = factory(User::class)->create();


            $faker = Factory::create();
            $browser->loginAs($user);
            $browser->assertAuthenticatedAs($user);

            $browser->visit('entry/create')
                ->assertSee('erstellen');

            $text = $faker->text(300).' new';
            $browser->loginAs($user->id)->visit('entry/create')
                ->type('description', $text)
                //     ->type('date', Carbon::yesterday()->toDateString())
                ->select('palaverItem_id')
                ->select('helper_id[]', User::inRandomOrder()->get()->first()->id)
                ->type('hours',-2)
                ->type('minutes', 20)
                ->press('Speichern')
                ->assertSee('403')
                ->assertSee('Negative Baustunden sind illegal');

            $newEntry = entry::whereDescription($text)->first();
            self::assertNull($newEntry);

        });
    }

    public function testCreate_negative_time_admin()
    {
        $this->browse(function (Browser $browser) {
            $user = factory(User::class, 'admin')->create();


            $faker = Factory::create();
            $browser->loginAs($user);
            $browser->assertAuthenticatedAs($user);

            $browser->visit('entry/create')
                ->assertSee('erstellen');

            $text = $faker->text(300).' new';
            $browser->loginAs($user->id)->visit('entry/create')
                ->type('description', $text)
                //     ->type('date', Carbon::yesterday()->toDateString())
                ->select('palaverItem_id')
                ->select('helper_id[]', User::inRandomOrder()->get()->first()->id)
                ->type('hours',-2)
                ->press('Speichern')
                ->assertSee('Eintrag gespeichert')
                ->assertSee('Arbeitszeit')
                ->assertSee('Eintrag erstellen');

            $newEntry = entry::whereDescription($text)->first();
            self::assertNotNull($newEntry);
            self::assertEquals($text, $newEntry->description);

        });
    }

    public function testAuthorizedDelete()
    {
        $this->browse(function (Browser $browser) {


            $user = factory(User::class)->create();
            $entry = factory(entry::class)->make();
            $entry->user_id = $user->id;
            $entry->save();

            $this->actingAs($user)->delete('/entry/'.$entry->id)->assertRedirect();

        });
    }

    public function testUnauthorizedDelete()
    {
        $this->browse(function (Browser $browser) {


            $user = factory(User::class)->create();
            $entry = factory(entry::class)->make();
            $entry->user_id = $user->id-1;
            $entry->save();
            $this->actingAs($user)->delete('/entry/'.$entry->id)->assertForbidden();

        });
    }
}
