<?php

namespace App\Http\Controllers;

use App\models\pilot;
use Illuminate\Http\Request;


class pilot_controller extends Controller {


    public function store(Request $request) {
        $pilot=new pilot();
        $pilot->fill($request->all());
        $this->authorize('store',$pilot);
        $pilot->save();
        return back();

    }

    public function destroy(pilot $pilot) {
        $this->authorize('destroy',$pilot);
        $pilot->delete();
        return back();
    }


}
