<?php

namespace App\Http\Requests;

use Carbon\Carbon;

class entry_request extends Request {


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return True;
        //Authorization logic shouldn't go here, because mocking of this method won't work for some reason
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {

        $validator->after(function ($validator) {

            // Muss manuell geprüft werden, da Regel nur required_if prüfen kann
            if(!empty($this->hours) && !is_numeric($this->hours)){
                $validator->errors()->add('hours', 'Willst du was eintragen musst du auch was machen');

            }

            if(!empty($this->minutes) && !is_numeric($this->minutes)){
                $validator->errors()->add('minutes', 'Willst du was eintragen musst du auch was machen');

            }

            if ($this->hours == 0 && $this->minutes == 0) {
                $validator->errors()->add('hours', 'Willst du was eintragen musst du auch was machen');
                $validator->errors()->add('minutes', 'Willst du was eintragen musst du auch was machen');

            }
        });
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages() {
        return [
                'date.before'             => 'Das Datum darf nicht in der Zukunft liegen',
                'date.after'              => 'Bitte keine Einträg für das vergangene Geschäftsjahr anlegen',
                'body.required'           => 'Bitte eine Beschreibung eingeben',
                'hours.min'               => 'Keinen Quatsch eingeben',
                'minutes.min'             => 'Keinen Quatsch eingeben',
                'hours.numeric'           => 'Keinen Quatsch eingeben',
                'minutes.numeric'         => 'Keinen Quatsch eingeben',
                'minutes.max'             => 'Keinen Quatsch eingeben',
                'palaverItem_id.required' => 'Bitte Aufgabe angeben..',
                'hours.required_if'       => 'Zeitangabe nicht vergessen',
                'minutes.required_if'     => 'Zeitangabe nicht vergessen',




        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {

        return [
                'user_id'        => 'required',
                'date'           => 'required|date|before:'.Carbon::tomorrow('Europe/Berlin').'|after:'.\Help::beginOfCurrentYear()->subDay(),
                'palaverItem_id' => 'required',
                'description'    => 'required|max:600|min:10',
                'hours'          => 'required_if:minutes,Null',
                'minutes'        => 'required_if:hours,Null'

        ];
    }
}
