<?php

namespace App\Providers;

use App\models\entry;
use Illuminate\Support\ServiceProvider;
use App\Observers\EntryObserver;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // ensure ssl in production
        if(config('app.env') === 'production') {
            \URL::forceScheme('https');
        }
        entry::observe(EntryObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
