<?php

namespace App\Policies;

use App\models\pilot;
use App\models\User;
use Carbon\Carbon;
use Illuminate\Auth\Access\HandlesAuthorization;

class pilotPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function create(User $user,$date) {

        if ($user->getFlugberechtigtAttribute(\Help::getYear(Carbon::parse($date)))) {
            return $this->allow();
        }
        return $this->deny("Du bist nicht Flugberechtigt");
    }


    public function destroy(User $user, pilot $pilot) {
        if($user->is_admin){
            return $this->allow();
        }

        //Check FK instead of retrieving model
        if(!($user->id == $pilot->user_id)){

            return $this->deny('Du darfst nur eigene Einträge löschen');
        }

        elseif(!Carbon::tomorrow()->lte(Carbon::parse($pilot->date))){
            return $this->deny('Das Datum liegt in der Vergangenheit');
        }

        return $this->allow();

    }

    public function store(User $user, pilot $pilot) {
        if($user->is_admin){
            return $this->allow();
        }

        //check for duplicate
        $duplicate=pilot::where('user_id',$user->id)->where('date',$pilot->date)->get()->count()>0;
        if($duplicate){
            return $this->deny('Du hast dich für heute bereits eingetragten');
        }
        elseif(!Carbon::parse($pilot->date)->gt(Carbon::today())){
            return $this->deny('Das Datum liegt in der Vergangenheit');
        }
        elseif(!$pilot->user->getFlugberechtigtAttribute(\Help::getYear(Carbon::parse($pilot->date)))){
            return $this->deny('Du bist nicht flugberechtigt');
        }

        return $this->allow();
    }
}
