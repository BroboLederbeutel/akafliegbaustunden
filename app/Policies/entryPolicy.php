<?php

namespace App\Policies;

use App\models\entry;
use App\models\User;
use Carbon\Carbon;
use Illuminate\Auth\Access\HandlesAuthorization;

class entryPolicy {
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct() {
        //
    }

    public function changing(User $user, entry $entry)
    {

        if ($entry->helpers->contains($user)) {
           return $this->deny('Du kannst nicht mit dir selbst zusammen arbeiten!');
        }
        else if ($entry->work_time > 720 && !$user->is_admin){
            return $this->deny('Das scheint mir nach einem verdächtig langem Tag');
        }

        else if(!($user->id == $entry->user_id) && $user->is_admin == 0) {
            return $this->deny('Du darfst nur an deinen eigenen Einträgen Änderungen vornehmen');
        }

        // wenn Eintrag älter als zwei Wochen
        else if (! Carbon::parse($entry->created_at)->gte(Carbon::now()->subWeeks(2))) {
            return $this->deny('Änderungen nur innerhalb von zwei Wochen möglich');
        }

        else if($entry->work_time < 0 && !$user->is_admin){
                return $this->deny('Negative Baustunden sind illegal');
        }
        else{
            return $this->allow('granted');
        }

    }

    public function store(User $user, entry $entry) {
        return $this->changing($user, $entry);

    }

    public function edit(User $user, entry $entry) {
        return $this->changing($user, $entry);

    }

    public function destroy(User $user, entry $entry){
        return $this->changing($user, $entry);
    }

    public function update(User $user, entry $entry) {
        if (empty($entry->created_at)) {
           return $this->deny();
        }

        return $this->changing($user, $entry);

    }

}
