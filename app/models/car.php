<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\models\car
 *
 * @property integer $id
 * @property integer $seats
 * @property string $date
 * @property string $comment
 * @property integer $user_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\models\User[] $mitfahrer
 * @property-read \App\models\User $fahrer
 * @method static \Illuminate\Database\Query\Builder|\App\models\car whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\models\car whereSeats($value)
 * @method static \Illuminate\Database\Query\Builder|\App\models\car whereDate($value)
 * @method static \Illuminate\Database\Query\Builder|\App\models\car whereComment($value)
 * @method static \Illuminate\Database\Query\Builder|\App\models\car whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\models\car whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\models\car whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read mixed $is_full
 */
class car extends Model {
    protected $table = 'car';
    protected $guarded = [];

    public function mitfahrer() {
        return $this->belongsToMany('\App\models\user', 'car_user', 'car_id', 'user_id');
    }

    public function fahrer() {
        return $this->hasOne('App\models\user', 'id', 'user_id');
    }

    public function getIsFullAttribute() {
        return $this->mitfahrer->count()==$this->seats;
    }
}
