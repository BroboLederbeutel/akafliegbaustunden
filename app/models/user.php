<?php

namespace App\models;


use Carbon\Carbon;
use Doctrine\DBAL\Query\QueryBuilder;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Mpociot\Versionable\VersionableTrait;
use Illuminate\Notifications\Notifiable;


/**
 * App\models\User
 *
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 * @property string $nickname
 * @property integer $status_id
 * @property boolean $next_palaver_entschuldigt
 * @property boolean $active
 * @property string $password
 * @property boolean $admin
 * @property boolean $palaver
 * @property boolean $flugverbot
 * @property boolean $flugverbot_kommentar
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\models\status $status
 * @property-read mixed $can_palaver
 * @property-read mixed $is_admin
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\models\entry[] $project_help
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\models\palaverItem[] $palaverItem_help
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\models\project[] $project
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\models\palaver[] $palaver_anwesend
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\models\palaver[] $palaver_entschuldigt
 * @property-read mixed $next_entschuldigt
 * @property-read mixed $full_name
 * @property-read mixed $active_string
 * @property-read mixed $short_name
 * @property-read mixed $palaver_item_string
 * @property-read mixed $formatted_work_time
 * @property-read mixed $total_work_time
 * @property-read mixed $flugberechtigt
 * @property-read mixed $entries_paginated
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\models\entry[] $entries
 * @property-write mixed $reason
 * @property-read \Illuminate\Database\Eloquent\Collection|\Mpociot\Versionable\Version[] $versions
 * @method static \Illuminate\Database\Query\Builder|\App\models\User whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\models\User whereFirstName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\models\User whereLastName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\models\User whereNickname($value)
 * @method static \Illuminate\Database\Query\Builder|\App\models\User whereStatusId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\models\User whereNextPalaverEntschuldigt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\models\User whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\App\models\User wherePassword($value)
 * @method static \Illuminate\Database\Query\Builder|\App\models\User whereAdmin($value)
 * @method static \Illuminate\Database\Query\Builder|\App\models\User wherePalaver($value)
 * @method static \Illuminate\Database\Query\Builder|\App\models\User whereRememberToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\models\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\models\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\models\User active()
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\models\car[] $cars
 * @property-read mixed $vacation
 * @property string $email
 * @method static \Illuminate\Database\Query\Builder|\App\models\User whereEmail($value)
 */
class User extends Authenticatable
{
    use VersionableTrait;
    use Notifiable;


    protected $table='users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
            'first_name', 'last_name', 'nickname', 'status_id', 'active','password'
    ];



    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
            'password', 'remember_token',
    ];

    protected $dontVersionFields = ['updated_at', 'remember_token'];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('orderBy', function ($builder) {
            $builder->orderBy('last_name', 'asc');
        });

        //hier kein global scope für miglieder der jungen Gruppe,
        //da ansonsten die dazugehörigen Einträge nicht angezeigt werden können und Fehler versucachen

    }

    public function status() {
        return $this->hasOne('App\models\status','id','status_id');
    }

    public function getCanPalaverAttribute() {
        return $this->palaver || $this->is_admin;
    }

    public function getIsAdminAttribute() {
        if(is_null($this->admin)){
            return 0;
        }
        return $this->admin;
    }

    public function project_help() {
        return $this->belongsToMany('App\models\entry', 'entry_helpers', 'user_id', 'entry');
    }

    public function palaverItem() {
        return $this->belongsToMany('App\models\palaverItem', 'palaverItem_user', 'user_id', 'palaverItem_id');
    }

    public function project() {
        return $this->hasMany('App\models\project', 'user_id', 'id');
    }

    public function palaver_anwesend() {
        return $this->belongsToMany('App\models\palaver', 'user_palaver_anwesend', 'user_id', 'palaver_id');
    }

    public function palaver_entschuldigt() {
        return $this->belongsToMany('App\models\palaver', 'user_palaver_entschuldigt', 'user_id', 'palaver_id');
    }
    //um anwesenheit zu speichern

    public function cars() {
        return $this->hasMany('App\models\car','user_id','id');
    }

    public function entries() {
        return $this->hasMany('App\models\entry')->orderBy('date', 'desc');
    }

    public function getVacationAttribute() {

        return $this->status->id==5; //Urlaub

    }

    /**
     * @param Palaver $palaver Das palaver, für das entschuldigt festgestellt werden soll
     *
     * @return bool ob der user bei gegebenem Palaver entschuldigt ist
     */
    public function entschuldigt($palaver) {
        return $palaver->entschuldigte->where('id', $this->id)->count() > 0;
        // return $this->palaver_entschuldigt->where('id', $palaver->id)->count() > 0;
    }

    /**
     * @param Palaver $palaver palaver, für das anwesend festgestellt werden soll
     *
     * @return bool ob der user bei gegebenem Palaver anwesend ist
     */
    public function anwesend($palaver) {
        return $palaver->anwesende->where('id', $this->id)->count() > 0;
        //  return $this->palaver_anwesend->where('id', $palaverId)->count() > 0;
    }

    /**
     * Gibt an, ob der user bei dem nächsten erstellten Palaver automatisch entschuldigt sein soll. Dies ist der Fall,
     * wenn er sich vor dem Palaver als fehlend gemeldet hat (next_palaver_entschuldigt=1) oder im Urlaub ist
     * @return boolean
     */
    public function getNextEntschuldigtAttribute() {
        //TODO return 1 bei urlaub
        return $this->next_palaver_entschuldigt;

    }

    //Aktivierte Mitglieder der aktiven Gruppe
    public function scopeActive(QueryBuilder $query) {
        return $query->where('active', 1);
    }

    public function scopeJungeGruppe(Builder $query) {
        $statuses = status::all();
        $id1 = $statuses->where('name', 'Ausgetreten')->first()->id;
        $id2 = $statuses->where('name', 'Alter Sack')->first()->id;

        return $query->where('status_id','NOT LIKE',$id1)->where('status_id','NOT LIKE',$id2);
    }

    public function scopeNonResigned(Builder $query)
    {
        $statuses = status::all();

        $id1 = $statuses->where('name', 'Ausgetreten')->first()->id;

        return $query->where('status_id','NOT LIKE', $id1);
    }


    /*
     * @return
     */
    public function getFullNameAttribute() {
        if (empty($this->nickname)) {
            return $this->first_name . " " . $this->last_name;
        } else {
            return $this->first_name . " \"" . $this->nickname . "\" " . $this->last_name;
        }
    }


    //gibt nur Aktivierte Nutzer, //TODO +die nicht beurlaubt sind

    public function getActiveStringAttribute() {
        return $this->active? 'ja':'nein';
    }

    public function getShortNameAttribute() {

        if (empty($this->nickname)) {
            return $this->first_name;
        }
        return $this->nickname;
    }

    public function getPalaverItemStringAttribute() {
        //gibt Projekte als String zurück
        $palaverItems = $this->palaverItem;

        if ($palaverItems->isEmpty()) {
            return "Aktuell keine Aufgaben";
        }
        $result = "";
        for ($x = 0; $x < sizeof($palaverItems) - 1; $x++) {
            $result = $result . '<a href=/palaverItem/' . $palaverItems[$x]->id . '>' . $palaverItems[$x]->project->name . ' - ' . $palaverItems[$x]->title . '</a>,';
        }
        return $result . '<a href=/palaverItem/' . $palaverItems[sizeof($palaverItems) - 1]->id . '>' . $palaverItems[sizeof($palaverItems) - 1]->project->name . ' - ' . $palaverItems[sizeof($palaverItems) - 1]->title . '</a>';


    }

    public function getFormattedWorkTimeAttribute($year) {

        $time = $this->getTotalWorkTimeAttribute($year);
        return \Help::format_time($time);
    }

    public function getTotalWorkTimeAttribute($year) {

        $dates = \Help::getStartEndDates($year);
        $start_date = $dates[0];
        $end_date = $dates[1];

        //filter entries that have been made in different years
        $entries = $this->entries->filter(function ($item) use ($start_date, $end_date) {
            $prev = Carbon::parse($item->date)->gte($start_date);
            $end = Carbon::parse($item->date)->lte($end_date);
            return $prev && $end; //keep item if changed in that time frame
        });

         return $entries->sum('work_time');
    }


    public function getWorkThisMonthAttribute()
    {
        return $this->entries->filter(function(entry $entry){
            return Carbon::parse($entry->date)->gte(Carbon::today()->startOfMonth());
        })->sum('work_time');


    }

    public function getFlugberechtigtAttribute($year) {
        if($this->flugverbot){
            return false;
        }

        if($this->status->name=='Alter Sack'){ //TODO nach String prüfen ist hässlich und dumm, aber neues Datenbankfeld machen ist auch irgendwie doof......
            return true;
        }

        if ($this->active) {
            return ($this->getRelativeWorkTime($year) >= 0);
        }

        return ($this->getRelativeWorkTime($year) > 0) && ($this->getTotalWorkTimeAttribute($year) > (60 * 50)); //Vorläufige Mitglieder müssen zuerst 50 Stunden arbeiten
    }

    public function getRelativeWorkTime($year) {

        return (int) $this->getTotalWorkTimeAttribute($year) - (int) ($this->getRequiredWorkTime($year) * 60);
    }

    //returns required work time in hours
    //changes in status take effect the following month
    //2015 wäre das Geschäftsjahr 1.11.15-30.10.16

    public function getRequiredWorkTime($year) {


        $dates = \Help::getStartEndDates($year);
        $start_date = $dates[0];
        $end_date = $dates[1];

        $versions = $this->versions;

        //filter changes that have been made in different years
        $versions = $versions->filter(function ($item) use ($start_date) {
            $prev = $item->created_at->gt($start_date);
            $follow = $item->created_at->lt($start_date->copy()->addYear());
            return $prev && $follow; //keep item if changed in that time frame
        });

        $required_hrs_month = $this->status->required_hrs;

        //if no changes have been made this year
        //if ($versions->isEmpty()) {


            if (!empty($this->created_at)) {//If statement ist workaround for manually created user.

                //wenn nutzer noch nicht beigetreten ist
                if ($this->created_at->gt($end_date)) {
                    return 0;
                }

                if ($this->created_at->gt($start_date)) {
                    $months = $this->created_at->diffInMonths($end_date);

                    return ($months * $required_hrs_month);
                }
                //if user has been a member since beginning of the year
                return $start_date->diffInMonths($end_date) * $required_hrs_month;
            }
            //TODO Für angefangenen Monat werden keine Baustunden verlangt

            //if created_at is null
            return $start_date->diffInMonths($end_date) * $required_hrs_month;

    }

    /*  Is this even used??
        /**
         * Paginated posts accessor. Access via $topic->posts_paginated
         *
         * @return \Illuminate\Pagination\Paginator
         */
    public function getEntriesPaginatedAttribute() {
        return $this->entries()->with('palaverItem', 'helpers')->paginate(15);
    }



    public function formattedRelativeWorkTime($year) {

        $time = $this->getRelativeWorkTime($year);
        return \Help::format_time($time);

    }

    /**
     * @param $date
     *
     * @return bool ob der User an dem Datum Auto fährt/mitfährt
     */
    public function drivesCar($date) {
        $cars=car::where('date',$date)->with('mitfahrer')->get();
        if($cars->count()==0){
            return false;
        }

        //Wenn user fahrer ist
        $filteredCars=$cars->filter(function(car $value,$key ){
            return $value->fahrer->id === $this->id;
        }
        );
        if ($filteredCars->count()!=0)
        {
            return true;
        }
        //Wenn user mitfahrer ist
        $passengers=collect();

        $cars->each(function(car $value) use ($passengers){
            $passengers->push($value->mitfahrer);

        });


        $passengers=$passengers->flatten();
        //Direkt User vergleichen geht irgendwie nicht
        $passengers=$passengers->map(function($value,$key){
            return $value->id;
        });

        return $passengers->contains($this->id);

    }


}
