<?php

use Illuminate\Database\Migrations\Migration;

class AddPalaveTagToUser extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        //gibt nutzer die Rechte Palaver zu erstellen
        Schema::table('users', function ($table) {

            $table->boolean('palaver')->after('admin')->default(0);

        }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('users', function ($table) {
            $table->dropColoumn('palaver');

        }
        );
    }
}
