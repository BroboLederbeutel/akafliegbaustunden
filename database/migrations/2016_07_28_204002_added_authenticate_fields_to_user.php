<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddedAuthenticateFieldsToUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function ($table) {
            $table->string('password',255)->after('active');
            $table->boolean('admin')->after('password')->default(0);
            $table->rememberToken()->after('admin');
        }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function ($table) {
            $table->dropColoumn('password');
            $table->dropColoumn('admin');
            $table->dropColoumn('remember_token');
        }
        );
    }
}
