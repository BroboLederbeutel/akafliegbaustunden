<?php

use Illuminate\Database\Seeder;

class projectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\models\project::class,20)->create()->each(function ($project){
          $faker = \Faker\Factory::create();

            //attach some PalaverItems
          for ($x = 0; $x<$faker->numberBetween(1,9); $x++){
                $project->palaverItems()->save(factory(\App\models\palaverItem::class)->make());
          }
    });
    }
}
